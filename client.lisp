(defpackage :zmq-rpc.client
  (:use :cl)
  (:export #:*rpc-server-address*
           #:rpc-call
           #:rpc-eval
           #:with-rpc-eval
           #:rpc-connect
           #:with-rpc-connection
           #:ping
           #:status))

(in-package :zmq-rpc.client)

(defvar *rpc-server-address* "tcp://localhost:5555")

(defun rpc-connect (address)
  (setf *rpc-server-address* address))

(defun call-with-rpc-connection (address function)
  (let ((*rpc-server-address* address))
    (funcall function)))

(defmacro with-rpc-connection (address &body body)
  `(call-with-rpc-connection ,address (lambda () ,@body)))

(defun rpc-call (fname &rest args)
  (pzmq:with-context (ctx)
    (pzmq:with-socket (requester ctx)
        (:req :affinity 3 :linger 100)
      (pzmq:connect requester *rpc-server-address*)
      (let ((fcall (flex:with-output-to-sequence (s)
                     (cl-store:store (list :call fname args) s))))
        (let ((msg (cffi:convert-to-foreign fcall (list :array :uint8 (length fcall)))))
          (pzmq:send requester msg
                     :len (length fcall))
          (unwind-protect
               (pzmq:with-message msg
                 (pzmq:msg-recv msg requester)
                 (let ((data (cffi:convert-from-foreign (pzmq:msg-data msg)
                                                        (list :array :uint8 (pzmq:msg-size msg)))))
                   (let ((result (flex:with-input-from-sequence (s data)
                                   (cl-store:restore s))))
                     (ecase (first result)
                       (:ok (apply #'values (rest result)))
                       (:error
                                        ;(error "RPC server error: ~A" (second result))
                        (error (second result))
                        )))))
            (cffi:foreign-free msg)))))))

(defun rpc-eval (code)
  (pzmq:with-context (ctx)
    (pzmq:with-socket (requester ctx)
        (:req :affinity 3 :linger 100)
      (pzmq:connect requester *rpc-server-address*)
      (let ((eval (flex:with-output-to-sequence (s)
                    (cl-store:store (list :eval code) s))))
        (let ((msg (cffi:convert-to-foreign eval (list :array :uint8 (length eval)))))
          (pzmq:send requester msg
                     :len (length eval))
          (unwind-protect
               (pzmq:with-message msg
                 (pzmq:msg-recv msg requester)
                 (let ((data (cffi:convert-from-foreign (pzmq:msg-data msg)
                                                        (list :array :uint8 (pzmq:msg-size msg)))))
                   (let ((result (flex:with-input-from-sequence (s data)
                                   (cl-store:restore s))))
                     (ecase (first result)
                       (:ok (apply #'values (rest result)))
                       (:error (error "RPC server error: ~A" (second result)))))))
            (cffi:foreign-free msg)))))))

(defmacro with-rpc-eval (&body body)
  `(rpc-eval '(progn ,@body)))

(defun ping ()
  (rpc-call "zmq-rpc::ping"))

(defun status ()
  (rpc-call "zmq-rpc::rpc-server-status"))
