(defpackage #:zmq-rpc
  (:use #:cl)
  (:export
   #:start-rpc-server
   #:start-logging
   #:stop-logging
   #:*catch-errors*))
