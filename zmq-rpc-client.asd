(asdf:defsystem #:zmq-rpc-client
  :description "RPC-ZMQ client for Common Lisp"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :serial t
  :components ((:file "client"))
  :depends-on (:cffi :pzmq :cl-store :flexi-streams))
