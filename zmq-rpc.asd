(asdf:defsystem #:zmq-rpc
  :description "RPC via ZMQ for Common Lisp"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :serial t
  :components ((:file "package")
               (:file "zmq-rpc"))
  :depends-on (:bordeaux-threads
               :cffi
               :pzmq
               :cl-store
               :flexi-streams
               :log5
               :trivial-backtrace))
