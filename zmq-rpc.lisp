(in-package #:zmq-rpc)

(log5:defcategory zmq-rpc)

(defun start-logging ()
  (log5:start-sender 'zmq-rpc
					 (log5:stream-sender :location *standard-output*)
					 :category-spec '(zmq-rpc log5:info+)
					 :output-spec '(log5:time log5:category log5:message)))

(defun stop-logging ()
  (log5:stop-sender 'zmq-rpc))

(defvar *server-address* "tcp://*:5555")

(defparameter *catch-errors* t)

(defun ping ()
  "Pong")

(defun rpc-server-status ()
  "Ok")

(defun call-with-rpc-error-handler (function receiver)
  (if (not *catch-errors*)
      (funcall function)
      (handler-case
          (funcall function)
        (error (e)
          (log5:log-for (zmq-rpc log5:error+) "ERROR: ~A" e)
          (log5:log-for (zmq-rpc log5:error+)
                        (trivial-backtrace:print-backtrace e :output nil))
          (let ((data (flex:with-output-to-sequence (s)
                        (cl-store:store (list :error e) s))))
            (let ((msg (cffi:convert-to-foreign data (list :array :uint8 (length data)))))
              (unwind-protect
                   (pzmq:send receiver msg
                              :len (length data))
                (cffi:foreign-free msg))))))))

(defmacro with-rpc-error-handler ((receiver) &body body)
  `(call-with-rpc-error-handler (lambda () ,@body) ,receiver))

(defun rpc-worker (&key (worker-address "inproc://rpc-workers"))
  (pzmq:with-socket receiver :rep
    (pzmq:connect receiver worker-address)
    (loop
       (with-rpc-error-handler (receiver) 
         (pzmq:with-message msg
           (pzmq:msg-recv msg receiver)
           (let* ((data (cffi:convert-from-foreign (pzmq:msg-data msg)
                                                   (list :array :uint8 (pzmq:msg-size msg))))
                  (msg-data (flex:with-input-from-sequence (buf data)
                              (cl-store:restore buf))))
             (ecase (first msg-data)
               (:call
                (destructuring-bind (_ fname args) msg-data
                  (declare (ignore _))
                  (log5:log-for (zmq-rpc log5:info+) "RPC: Calling: ~A ~A" fname args)
                  (let* ((function (symbol-function (cond
                                                      ((stringp fname)
                                                       (read-from-string fname))
                                                      ((symbolp fname)
                                                       fname)
                                                      (t (error "Invalid rpc call")))))
                         (result (apply function args))
                         (result-data (flex:with-output-to-sequence (s)
                                        (cl-store:store (list :ok result) s)))
                         (result-msg (cffi:convert-to-foreign result-data (list :array :uint8 (length result-data)))))
                    (unwind-protect
                         (pzmq:send receiver result-msg
                                    :len (length result-data))
                      (cffi:foreign-free result-msg)
                      ))))
               (:eval
                (destructuring-bind (_ eval) msg-data
                  (declare (ignore _))
                  (let* ((code (if (stringp eval)
                                   (read-from-string eval)
                                   eval))
                         (result (eval code))
                         (result-data (flex:with-output-to-sequence (s)
                                        (cl-store:store (list :ok result) s)))
                         (result-msg (cffi:convert-to-foreign result-data (list :array :uint8 (length result-data)))))
                    (unwind-protect
                         (pzmq:send receiver result-msg
                                    :len (length result-data))
                      (cffi:foreign-free result-msg)
                      ))))
               )))
         ))))

(defun start-rpc-server (&key (listen-address *server-address*)
                           (workers-address "inproc://rpc-workers")
                           (nthreads 3))
  (pzmq:with-sockets ((clients :router) (workers :dealer))
    (pzmq:bind clients listen-address)
    (pzmq:bind workers workers-address)
    (let ((threads (list)))
      (unwind-protect
           (progn
             (dotimes (i nthreads)
               (push (bt:make-thread #'rpc-worker
                                     :name (format nil "rpc-worker~d" i))
                     threads))
             (pzmq:device :queue clients workers))
        (map 'nil #'bt:destroy-thread threads)))))
